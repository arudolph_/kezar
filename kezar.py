#!/usr/bin/env python

import bs4
import datetime
import re
import requests
import subprocess
import tempfile
import time

# edit as desired
RECIPIENTS = ['arudolph@gmail.com']
# 0 => Monday
DAY_OF_WEEK = 2
TRACK_TIME = ['7:30 AM', '8:30 AM']


def get_url(url):
    ua = ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit'
          '/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36')
    return requests.get(url, headers={'user-agent': ua})


def get_pdf_url():
    url = ('https://sfrecpark.org/permits-and-reservations/'
           'stadium-rentals/kezar-events/')
    r = get_url(url)
    soup = bs4.BeautifulSoup(r.text, 'html.parser')
    a = soup.find('a', text=re.compile(r'^Kezar Stadium Schedule'))
    return a['href']


def get_events():
    url = get_pdf_url()

    with tempfile.NamedTemporaryFile() as f:
        f.write(get_url(url).content)
        p = subprocess.run(['pdftotext', f.name, '-'], stdout=subprocess.PIPE)
        pdf_text = p.stdout.decode()

    # pdftotext -> {day1}\n{day2}\n\n{time1}\n{time2}\n\n{event1}\n{event2}
    field = 0
    values = [[], [], []]
    for l in pdf_text.split('\n')[1:]:
        l = l.strip()
        if l:
            values[field ].append(l)
        else:
            field += 1
            if field >= len(values):
                break

    return list(zip(*values))


def time_overlaps(t):
    def convert(t): return time.strptime(t.strip(), '%I:%M %p')

    s, e = [convert(i) for i in t.split('–')]
    ts, te = [convert(i) for i in TRACK_TIME]
    return ts < s < te or ts < e < te


def send_mail(event):
    result = subprocess.run(
        ['mail', '-s', '[trackbot] kezar conflict'] + RECIPIENTS,
        input=event,
        encoding='utf8',
        check=True
    )
    print(f'mail sent ({event})')


def main():
    events = get_events()

    today = datetime.date.today()
    delta = datetime.timedelta((DAY_OF_WEEK - today.weekday()) % 7)
    next_track_day = today + delta
    # REVIEW: non-0-padded day-of-month with strfime?
    dstr = next_track_day.strftime('%A, %B ') + str(next_track_day.day)
    for d, t, e in events:
        if d == dstr and time_overlaps(t):
            send_mail(' '.join((d, t, e)))


if __name__ == '__main__':
    main()
